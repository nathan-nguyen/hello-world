if [ "$#" -ne 2 ]; then
  echo "Usage: ./stupidclone.sh <url> <dirname>"
  exit
fi

git clone $1 $2 && \
  cd $2 && \
  git config user.name "nathan-nguyen" && \
  git config user.email "minhnhat.nguyen@uqconnect.edu.au"
